/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* gnome-crash - small utility that displays a backtrace after a core dump.
 *
 * Copyright (C) 2000 Jose M Celorio
 *
 * Author:  Chema Celorio <chema@celorio.com>
 *
 * code taken from gedit 0.9.0pre2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#include <config.h>
#include <gnome.h>
#include <sys/stat.h> /* for stat() */

#include <gnome-crash.h>

static GtkWidget *save_file_selector = NULL;

static void gnome_crash_file_save_as_ok_sel (GtkWidget *w, gpointer cbdata);
static void gnome_crash_file_open_ok_sel (GtkWidget *w, GtkWidget *open_file_selector);
static gint gnome_crash_file_open_real (const gchar *fname);

static void
cancel_cb (GtkWidget *w, gpointer data)
{
	gtk_widget_hide (data);
}

static gint
delete_event_cb (GtkWidget *widget, GdkEventAny *event)
{
	gtk_widget_hide (widget);
	
	return TRUE;
}

static gint
gnome_crash_file_selector_key_event (GtkFileSelection *fsel, GdkEventKey *event)
{
	if (event->keyval == GDK_Escape) {
		gtk_button_clicked (GTK_BUTTON (fsel->cancel_button));
		return 1;
	} else
		return 0;
}


static void
gnome_crash_file_save_as (const gchar *buffer, gint buffer_length)
{
	g_return_if_fail (buffer != NULL);
	
	if (save_file_selector && GTK_WIDGET_VISIBLE (save_file_selector))
		return;

	if (save_file_selector == NULL)
	{
		save_file_selector = gtk_file_selection_new (NULL);
		
		gtk_signal_connect(GTK_OBJECT(save_file_selector),
				   "delete_event",
				   GTK_SIGNAL_FUNC(delete_event_cb),
				   save_file_selector);
		gtk_signal_connect (GTK_OBJECT(GTK_FILE_SELECTION(save_file_selector)->cancel_button),
				    "clicked",
				    GTK_SIGNAL_FUNC(cancel_cb),
				    save_file_selector);
		gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION(save_file_selector)),
				    "key_press_event",
				    GTK_SIGNAL_FUNC (gnome_crash_file_selector_key_event),
				    NULL);
		gtk_signal_connect (GTK_OBJECT(GTK_FILE_SELECTION(save_file_selector)->ok_button),
				    "clicked",
				    GTK_SIGNAL_FUNC (gnome_crash_file_save_as_ok_sel),
				    buffer);

	}

	gtk_window_set_title (GTK_WINDOW(save_file_selector), _("Save As..."));

	if (!GTK_WIDGET_VISIBLE (save_file_selector))
	{
		gtk_window_position (GTK_WINDOW (save_file_selector), GTK_WIN_POS_MOUSE);
		gtk_widget_show(save_file_selector);
	}
	
	return;
}

static gboolean
gnome_crash_file_save_real (const gchar *buffer, gint buffer_length, const gchar *fname)
{
	FILE  *file_pointer;
	gchar *temp = NULL;

	g_return_val_if_fail (buffer != NULL, FALSE);

	if (fname == NULL) {
		gnome_crash_file_save_as (buffer, buffer_length);
		return TRUE;
	}

	if ((file_pointer = fopen (fname, "w")) == NULL)
	{
		gchar *errstr = g_strdup_printf (_("Unable to save the file: "
						   "\n\n %s \n\n"
						   "Make sure that the path you provided exists,"
						   "and that you have the appropriate write permissions."), fname);
		gnome_app_error (gnome_crash_get_toplevel (NULL), errstr);
		g_free (errstr);
		return FALSE;
	}
	
	if (fputs (buffer, file_pointer) == EOF)
	{
		gchar *errstr = g_strdup_printf (_("Unable to save the file :"
						   "\n\n %s \n\n"
						   "Because of an unknown reason (1). Please report this "
						   "Problem to submit@bugs.gnome.org"), fname);
		gnome_app_error (gnome_crash_get_toplevel (NULL), errstr);
		fclose (file_pointer);
		g_free (errstr);
		return FALSE;
	}

	if (fclose (file_pointer) != 0)
	{
		gchar *errstr = g_strdup_printf (_("Unable to save the file :"
						   "\n\n %s \n\n"
						   "Because of an unknown reason (2)."
						   "Please report this "
						   "Problem to submit@bugs.gnome.org"), fname);
		gnome_app_error (gnome_crash_get_toplevel (NULL), errstr);
		g_free (errstr);
		return FALSE;
	}

	if (gdb_info.file_name)
		temp = gdb_info.file_name;
	
	gdb_info.file_name = g_strdup (fname);

	if (temp)
		g_free (temp);
		
	return TRUE;
}


static void
gnome_crash_file_save_as_ok_sel (GtkWidget *w, gpointer cbdata)
{
	gchar *buffer;
	gchar *file_name;
	gint buffer_length;

	buffer = cbdata;

	g_return_if_fail (buffer != NULL);
	
	buffer_length = strlen (buffer);
	
	file_name = g_strdup(gtk_file_selection_get_filename (GTK_FILE_SELECTION(save_file_selector)));
	
	gtk_widget_hide (GTK_WIDGET (save_file_selector));
	/* hmmm ?? this looks like a memleak  to me ... Chema */
	save_file_selector = NULL;
	
        if (g_file_exists (file_name))
	{
		guchar * msg;
		GtkWidget *msgbox;
		gint ret;
		msg = g_strdup_printf (_("``%s'' is about to be overwritten. Do you want to continue ?"), file_name);
		msgbox = gnome_message_box_new (msg, GNOME_MESSAGE_BOX_QUESTION, GNOME_STOCK_BUTTON_YES,
						GNOME_STOCK_BUTTON_NO, GNOME_STOCK_BUTTON_CANCEL, NULL);
		gnome_dialog_set_default (GNOME_DIALOG (msgbox), 2);
		ret = gnome_dialog_run_and_close (GNOME_DIALOG (msgbox));
		g_free (msg);
		switch (ret)
		{
		case 0:
			break;
		default:
			return;
		}
	}
	    
	if (gnome_crash_file_save_real (buffer, buffer_length, file_name) != 0) {
		g_free (file_name);
		return;
	}

	g_free (file_name);
	g_warning ("Error while trying to save as ..\n");
}



void
gnome_crash_file_save (void)
{
	gchar *buffer;
	gchar *file_name;
	gint buffer_length;

	buffer        = gdb_info.buffer;
	buffer_length = gdb_info.buffer_length;
	file_name     = gdb_info.file_name;

	gnome_crash_file_save_real (buffer, buffer_length, file_name);

}



void
gnome_crash_file_open (void)
{
	static GtkWidget *open_file_selector = NULL;

	if (open_file_selector && GTK_WIDGET_VISIBLE (open_file_selector))
		return;

	if (open_file_selector == NULL)
	{
		open_file_selector = gtk_file_selection_new(NULL);

		gtk_signal_connect(GTK_OBJECT(open_file_selector),
				   "delete_event",
				   GTK_SIGNAL_FUNC (delete_event_cb),
				   open_file_selector);
		gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(open_file_selector)->cancel_button),
				   "clicked",
				   GTK_SIGNAL_FUNC (cancel_cb),
				   open_file_selector);
		gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION(open_file_selector)),
				    "key_press_event",
				    GTK_SIGNAL_FUNC (gnome_crash_file_selector_key_event),
				    NULL);
		gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(open_file_selector)->ok_button),
				   "clicked",
				   GTK_SIGNAL_FUNC (gnome_crash_file_open_ok_sel),
				   open_file_selector);
	}

	gtk_window_set_title (GTK_WINDOW(open_file_selector), _("Open File ..."));

	if (!GTK_WIDGET_VISIBLE (open_file_selector))
	{
		gtk_window_position (GTK_WINDOW (open_file_selector), GTK_WIN_POS_MOUSE);
		gtk_widget_show(open_file_selector);

		gtk_entry_set_editable(GTK_ENTRY (GTK_FILE_SELECTION(open_file_selector)->selection_entry), FALSE);
	}

	return;
}



static void
gnome_crash_file_open_ok_sel (GtkWidget *w, GtkWidget *open_file_selector)
{
	gchar *file_name;

	file_name = g_strdup(gtk_file_selection_get_filename (GTK_FILE_SELECTION(open_file_selector)));
	
	gtk_widget_hide (GTK_WIDGET (open_file_selector));

	/* hmmm ?? this looks like a memleak  to me ... Chema */
	/* more like a resource leak, I guess ... Diego */
	open_file_selector = NULL;
	
	g_return_if_fail (g_file_exists (file_name));

	/* we don't want to process directories, just plain files */
	if (g_file_test (file_name, G_FILE_TEST_ISDIR)) {
		g_free (file_name);
		return;
	}
	
	if (gnome_crash_file_open_real (file_name) != 0) {
		g_free (file_name);
		g_warning ("Error while trying to open ..\n");
		return;
	}

	g_free (file_name);
}





/**
 * gnome_crash_file_open_real
 * @fname: Filename to open
 *
 * Open a file and fill the global gdbInfo struct
 *
 * Return value: 0 on success, 1 on error.
 */
static gint
gnome_crash_file_open_real (const gchar *fname)
{
	gchar *tmp_buf;
	struct stat stats;
	FILE *fp;
	
	guint buffer_length;
	guint buffer_used;
	
	g_return_val_if_fail (fname != NULL, 1);
	
	if (stat(fname, &stats) ||  !S_ISREG(stats.st_mode))
	{
		gchar *errstr = g_strdup_printf (_("An error was encountered while opening the file \"%s\"."
							      "\nPlease make sure the file exists."), fname);
		gnome_app_error (gnome_crash_get_toplevel (NULL), errstr);
		g_free (errstr);
		return 1;
	}

	if (stats.st_size  == 0)
	{
		gchar *errstr = g_strdup_printf (_("An error was encountered while opening the file:\n\n%s\n\n"
						    "\nPlease make sure the file is not being used by another application\n"
						    "and that the file is not empty."), fname);
		gnome_app_error (gnome_crash_get_toplevel (NULL), errstr);
		g_free (errstr);
		return 1;
	}

	/* FIXME: is this right or should I use a multiple
	   of GNOME_CRASH_BUFFER_SIZE? */
	buffer_length = stats.st_size + 1;

	if ((tmp_buf = g_new0 (gchar, buffer_length)) == NULL)
	{
		gnome_app_error (gnome_crash_get_toplevel (NULL), _("Could not allocate the required memory."));
		return 1;
	}

	if ((fp = fopen (fname, "r")) == NULL)
	{
		gchar *errstr = g_strdup_printf (_("gnome-crash was unable to open the file: \n\n%s\n\n"
						   "Make sure that you have permissions for opening the file."), fname);
		gnome_app_error (gnome_crash_get_toplevel (NULL), errstr);
		g_free (errstr);
		return 1;
	}

	buffer_used = fread (tmp_buf, 1, stats.st_size, fp);
	fclose (fp);

	g_return_val_if_fail (buffer_used >= 0, 1);

	gnome_crash_gdb_info_clear ();

	/* just fill in GdbInfo buffer stuff */
	gdb_info.buffer = tmp_buf;
	gdb_info.file_name = g_strdup (fname);

	/* the next assignment is not a typo, just trying to be coherent
	   with gnome_crash_clean_backtrace in gnome-crash-gdb.c */
	gdb_info.buffer_length = buffer_used;  

	gdb_info.buffer_used = buffer_used;

	gdb_info.buffer[buffer_used] = 0;

	/* Refresh info showed in main window */
	gnome_crash_show_buffer ();
	
	gtk_idle_add ((GtkFunction) gnome_crash_function_load_all, NULL);

	/* tmp_buf shouldn't be freed as we're holding a pointer to it in
	   gdb_info.buffer */

	return 0;
}






