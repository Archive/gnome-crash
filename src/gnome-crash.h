/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_CRASH_H__
#define __GNOME_CRASH_H__

#include <glade/glade.h>
#include <sys/types.h>

#define GNOME_CRASH_GLADE_FILE       "gnome-crash.glade"
#define GNOME_CRASH_ICON             "gnome-crash-icon.png"
#define GNOME_CRASH_GLADE_FILE_ABOUT "about.glade"
#define GNOME_CRASH_GDB_FILE         "gdb-cmd"

typedef struct {
	/* gdb sutff */
	pid_t       app_pid;
	pid_t       gdb_pid;
	GIOChannel *ioc;
	int         fd;
	gboolean    explicit_dirty;

	/* Buffer stuff */
	gchar      *buffer;
	gchar      *file_name;
	guint       buffer_length;
	guint       buffer_used;
	guint       buffer_locals_offset;

	GList *functions; 
} GdbInfo;

typedef struct {
	/* package page */
	gchar *package;
	gchar *package_ver;
	
	/* dialog page */
	gchar *app_file;
	gchar *pid;
	
	/* core page */
	gchar *core_file;
} PoptData;

typedef struct {
	gchar *name;
	gint  number;

	guint offset;

	gchar *variables;
	guint  variables_length;
} GnomeCrashFunction;

extern PoptData  popt_data;
extern GdbInfo    gdb_info;

extern GladeXML  * gui;
extern GtkWidget * app;
extern GtkWidget * stack_box;
extern GtkWidget * variables_box;
extern GtkWidget * functions_list;


GnomeApp * gnome_crash_get_toplevel (GnomeApp *app_in);

GtkWidget * gnome_crash_animator_create (gchar *widget_name, gchar *imgname,
					 gchar *string2, gint size, gint freq);
void	gnome_crash_get_trace_from_pid (const gchar *app, const gchar *extra);
void	gnome_crash_gdb_info_clear (void);

/* gnome_crash_functions */
gint	gnome_crash_function_load_all (void);
void	gnome_crash_function_connect_signals (void);

/* gnome_crash_print */
void	gnome_crash_print_cb  (GtkWidget *widget, gpointer data);
void	gnome_crash_print_preview_cb (GtkWidget *widget, gpointer data);
#if 0 /* Why do we need this here ? */
void	gnome_crash_print_execute (gchar *buffer, gint file_printpreview);
#endif

/* gnome_crash_gui */
void	gnome_crash_show_buffer (void);

void gnome_crash_file_save (void);
void gnome_crash_file_open (void);

#endif /* __gnome_crash_h__ */



