/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* gnome-crash - small utility that displays a backtrace after a core dump.
 *
 * Copyright (C) 2000 Jose M Celorio
 *
 * Author:  Chema Celorio <chema@celorio.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include <config.h>
#include <gnome.h>

#include "gnome-crash.h"

static const gchar* ignored_functions[] = {
	"gnome_segv_handle",
	"__wait4",
	"__restore",
	"__DTOR_END__",
	NULL
};

static gboolean 
gnome_crash_function_ignore (gchar* function_name)
{
	gint n = 0;
	
	while (ignored_functions[n]!=NULL) {
		if (strcmp(function_name, ignored_functions[n])==0)
			return TRUE;
		n++;
	}
	
	return FALSE;
}
	

static void
gnome_crash_function_add_to_clist (GnomeCrashFunction *function)
{
	gint row;
	gchar *name_array[2];
	
	name_array [0] = function->name;
	name_array [1] = NULL;
	
	row = gtk_clist_append (GTK_CLIST (functions_list), name_array);
	gtk_clist_set_row_data (GTK_CLIST(functions_list), row, (gpointer) function);
}

static void
gnome_crash_function_add (gchar* name, guint offset, gint number)
{
	GnomeCrashFunction *function;

	if (gnome_crash_function_ignore (name))
		return;

	function = g_new (GnomeCrashFunction, 1);

	function->name   = g_strdup (name);
	function->offset = offset;
	function->number = number;

	gdb_info.functions = g_list_append (gdb_info.functions, function);

	gnome_crash_function_add_to_clist (function);
}

static void
gnome_crash_function_load_variables (gpointer data, guint *pOffset)
{
	gchar *search_text;
	GnomeCrashFunction *function;
	
	function = data;

	search_text = g_strdup_printf ("%i", function->number);
	while (*pOffset < gdb_info.buffer_length) {
		if ( gdb_info.buffer[*pOffset] == '#') {
			gint i;
			gchar *num = g_malloc (4);
			for (i=0;i<4;i++) {
				if (gdb_info.buffer[++(*pOffset)]==' ')
					break;
				num[i]=gdb_info.buffer[*pOffset];
			}
			num[i]=0;
			if ( strcmp (num, search_text) == 0) {
				g_free (num);
				function->offset = *pOffset - i - 1;
				break;
			}
			g_free (num);
		}
		(*pOffset)++;
	}
	g_free (search_text);
	
	while (*pOffset < gdb_info.buffer_length)
		if (gdb_info.buffer [++(*pOffset)] == '#')
			break;

	if (*pOffset > gdb_info.buffer_length) {
		function->variables = g_strdup ( _("No info."));
		function->variables_length = strlen (function->variables);
	} else {
		function->variables_length = *pOffset - function->offset;
		function->variables = g_strndup ( gdb_info.buffer + function->offset, function->variables_length);
	}
}

#define GNOME_CRASH_SIZE_NUM_MAX 4

static void
gnome_crash_function_load_functions (void)
{
	gchar num [GNOME_CRASH_SIZE_NUM_MAX + 1];
	gchar *buffer;
	gint p1;
	gint length;
	gint search_for_function = FALSE;
	gint function_number = 0;

	buffer = gdb_info.buffer;
	length = gdb_info.buffer_length;

	for (p1 = 0; p1 < length; p1++) {
		if ( buffer[p1] == '#') {
			gint i;
			for (i = 0; i < GNOME_CRASH_SIZE_NUM_MAX; i++) {
				if (buffer[p1+i+1]==' ')
					break;
				num[i]=buffer[p1+i+1];
			}
			num[i]=0;
			/* We use p1 > 100 since we don't want the
			   first occurence of "# 0" and we know that
			   if it is < 100, it will be the first one */
			if (strcmp(num,"0")==0 && p1 > 100) {
				/* advance p1 to the end so that we stop
				   searching for more functions */
				gdb_info.buffer_locals_offset = p1-1;
				p1 = length;
				search_for_function = FALSE;
			} else {
				search_for_function = TRUE;
			}
			function_number = atoi(num);
		}

		if (search_for_function) {
			if (buffer [p1-1] == 'i' &&
			    buffer [p1]   == 'n') {
				gint name_length;
				gchar *name;
				
				name_length = 0;
				p1++;
				p1++;
				while (buffer[p1+name_length]!=' ' && (p1+name_length) < length ) {
					name_length++;
				}
				name = g_strndup (buffer + p1, name_length);
				gnome_crash_function_add (name, p1, function_number);
				g_free (name);
				search_for_function = FALSE;
			}
		}
	}
}

static void
gnome_crash_function_clicked (GtkCList *clist, GdkEventButton *event, gpointer data)
{
	gint row;
	gint column;
	GnomeCrashFunction *function;
	gint dummy_pos;

	/* Verify that the button clicked is the rigth button */
	if (event->button != 1)
		return;

	/* if this is a double click, do nothing */
	if (event->type != GDK_BUTTON_PRESS)
		return;

	if (!gtk_clist_get_selection_info (clist, event->x, event->y, &row, &column))
		return;

	function = gtk_clist_get_row_data (clist, row);

	gtk_editable_delete_text (GTK_EDITABLE (variables_box), 0, -1);
	dummy_pos = 0;
	gtk_editable_insert_text (GTK_EDITABLE (variables_box),
				  function->variables,
				  function->variables_length,
				  &dummy_pos);
	dummy_pos = 0;
	gtk_editable_insert_text (GTK_EDITABLE (variables_box), " ", 1, &dummy_pos);
	gtk_editable_delete_text (GTK_EDITABLE (variables_box), 0, 1);

}


gint
gnome_crash_function_load_all (void)
{
	guint offset;

	/* clean info associated with each row of functions_list */
	gtk_clist_clear (GTK_CLIST (functions_list));
	
	gnome_crash_function_load_functions ();

	offset = gdb_info.buffer_locals_offset;

	g_list_foreach (gdb_info.functions, gnome_crash_function_load_variables,
			&offset);
	
	/* clean variables box */
	gtk_editable_delete_text (GTK_EDITABLE (variables_box), 0, -1);

	return FALSE;
}
 

void
gnome_crash_function_connect_signals (void) 
{
	gtk_signal_connect (GTK_OBJECT (functions_list), "button_press_event",
			    GTK_SIGNAL_FUNC (gnome_crash_function_clicked), 
			    NULL);
	
} 









