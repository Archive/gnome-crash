/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gnome.h>

#include <sys/wait.h>
#include <unistd.h>

#include "gnome-crash.h"

#define GNOME_CRASH_BUFFER_SIZE 1024

GtkWidget *dialog;
GtkWidget *animator;


#define GNOME_CRASH_NUM_LENGTH 4

/**
 * gnome_crash_clean_backtrace:
 * @void: 
 * 
 * Remove the trailing stuff at the end of the stacktrace
 * that contains no info.
 **/
static void
gnome_crash_clean_backtrace (void)
{
	gchar num [GNOME_CRASH_NUM_LENGTH + 1];
	gchar *buffer;
	gint p1;
	gint length;
	gint occurence;
	
	occurence = 0;

	buffer = gdb_info.buffer;
	length = gdb_info.buffer_length;
	
	for (p1 = 0; p1 < length; p1++) {
		if (buffer [p1] == '#') {
			gint i;
			/* Get the number after the '#' */
			for (i = 0; i < GNOME_CRASH_NUM_LENGTH; i++) {
				if (buffer[p1+i+1]==' ')
					break;
				num[i]=buffer[p1+i+1];
			}
			num[i]=0;
			/* Seach for the forth occurence of "#0" */
			if (strcmp(num,"0")==0) {
				occurence++;
				if (occurence == 4) {
					gdb_info.buffer [p1] = 0;
					gdb_info.buffer_length = p1;
				}
			}
		}
	}
}

static void
stop_gdb ()
{
	if (!gdb_info.ioc) {
		g_message ("gdb has already exited");
		return;
	}
	
	g_io_channel_close (gdb_info.ioc);
	waitpid (gdb_info.gdb_pid, NULL, 0);
	
	gdb_info.gdb_pid = 0;
	gdb_info.buffer [gdb_info.buffer_used] = 0;
	gdb_info.fd = 0;
	gdb_info.ioc = NULL;

	gnome_animator_stop (GNOME_ANIMATOR (animator));
	gtk_widget_destroy (dialog);
	
	gdb_info.buffer_length = gdb_info.buffer_used;
	gnome_crash_clean_backtrace ();

	gnome_crash_show_buffer ();

	gtk_idle_add ((GtkFunction) gnome_crash_function_load_all, NULL);

	return;
}

static gboolean
handle_gdb_input (GIOChannel *ioc, GIOCondition condition, gpointer data)
{	
	gchar buf [GNOME_CRASH_BUFFER_SIZE];
	guint len;

	if (condition == G_IO_HUP) {
		stop_gdb ();
		return FALSE;
	}

gdb_try_read:
	switch (g_io_channel_read (ioc, buf, 1024, &len)) {
	case G_IO_ERROR_NONE:
		break;
	case G_IO_ERROR_AGAIN:
		goto gdb_try_read;
	default:
		g_warning ("Error on read... aborting");
		stop_gdb ();
		return FALSE;
	}


	if (gdb_info.buffer_used + len > gdb_info.buffer_length)
	{
		gdb_info.buffer_length += GNOME_CRASH_BUFFER_SIZE;
		gdb_info.buffer = g_realloc (gdb_info.buffer,
					     gdb_info.buffer_length);
	}
	    

	memcpy (gdb_info.buffer + gdb_info.buffer_used,	buf, len);

	gdb_info.buffer_used += len;

	if (len == GNOME_CRASH_BUFFER_SIZE)
		gdb_info.buffer_length += GNOME_CRASH_BUFFER_SIZE;

	return TRUE;
}

static pid_t
start_commandv (const char *args[], int *rfd)
{
	int fd[2];
	pid_t pid;
	GtkWidget *d;

	if (pipe (fd) == -1) {
		perror ("can't open pipe");
		d = gnome_error_dialog (_("Unable to open pipe"));
		gnome_dialog_run_and_close (GNOME_DIALOG (d));
		return -1;
	}

	pid = fork ();
	if (pid == 0) {
		close (1);
		close (fd[0]);
		dup (fd[1]);

		execvp (args[0], (char **)args);
		g_warning (_("Could not run '%s'."), args[0]);
		_exit (1);
	} else if (pid == -1) {
		d = gnome_error_dialog (_("Error on fork()."));
		gnome_dialog_run_and_close (GNOME_DIALOG (d));
		close (fd[0]);
		close (fd[1]);
		return -1;
	}

	close (fd[1]);
	*rfd = fd[0];

	return pid;
}


GtkWidget *
gnome_crash_animator_create (gchar *widget_name, gchar *imgname, 
			     gchar *string2, gint size, gint freq)
{
	gchar *image_location;
	
	image_location = g_strdup_printf ("%s%s", GNOME_CRASH_DATA_DIR, imgname);

	animator = gnome_animator_new_with_size (48, 48);

	gnome_animator_set_loop_type (GNOME_ANIMATOR (animator),
				      GNOME_ANIMATOR_LOOP_PING_PONG);
	
	gnome_animator_append_frames_from_file (GNOME_ANIMATOR (animator),
						image_location, 0, 0, freq, size);

	if (image_location != NULL)
		g_free (image_location);
	
	return animator;
}

static void
gnome_crash_gdb_load_dialog (void)
{
	dialog = glade_xml_get_widget (gui, "dialog");

	g_return_if_fail (dialog != NULL);

	gnome_dialog_set_parent (GNOME_DIALOG(dialog), GTK_WINDOW(app));

	gtk_widget_show (dialog);
}


void
gnome_crash_get_trace_from_pid (const gchar *app, const gchar *extra)
{
	GtkWidget *d;
	int fd;
	const char *args[] = { "gdb", 
			       "--batch", 
			       "--quiet",
			       "--command=" GNOME_CRASH_GDB_DIR GNOME_CRASH_GDB_FILE,
			       NULL, NULL, NULL };

	args[4] = app;
	args[5] = extra;

	if (!app || !extra || !app[0] || !extra[0])
		return;

	if (!g_file_exists (GNOME_CRASH_GDB_DIR GNOME_CRASH_GDB_FILE))
	{
		g_warning ("Could not find %s", GNOME_CRASH_GDB_DIR GNOME_CRASH_GDB_FILE);
		return;
	}

	gnome_crash_gdb_load_dialog ();
	gnome_animator_start (GNOME_ANIMATOR (animator));

	gdb_info.gdb_pid       = start_commandv (args, &fd);
	gdb_info.file_name     = NULL;
	gdb_info.buffer_length = GNOME_CRASH_BUFFER_SIZE;
	gdb_info.buffer_used   = 0;
	gdb_info.buffer        = g_malloc (gdb_info.buffer_length);
	
	if (gdb_info.gdb_pid == -1) {
		d = gnome_error_dialog (_("Error on fork()."));
		gnome_dialog_run_and_close (GNOME_DIALOG (d));
		return;
	}
	
	gdb_info.fd = fd;
	gdb_info.ioc = g_io_channel_unix_new (fd);
	g_io_add_watch (gdb_info.ioc, G_IO_IN | G_IO_HUP, 
			handle_gdb_input, NULL);
	g_io_channel_unref (gdb_info.ioc);
	
	/*
	gtk_editable_delete_text (GTK_EDITABLE (GDB_TEXT), 0, -1);
	gnome_druid_set_buttons_sensitive (GNOME_DRUID (THE_DRUID),
					   FALSE, FALSE, TRUE);
	gnome_animator_start (GNOME_ANIMATOR (GDB_ANIM));
	gtk_widget_set_sensitive (GTK_WIDGET (STOP_BUTTON), TRUE);
	gtk_widget_set_sensitive (GTK_WIDGET (REFRESH_BUTTON),
				  FALSE);
	*/

	gdb_info.explicit_dirty = FALSE;
}


/* clean_gdb_info:
   clean up the global 
   structure gdb_info. */
void
gnome_crash_gdb_info_clear (void) 
{
	GList *tmp_list;

	/* gdb stuff */
	gdb_info.app_pid = 0;            /* seems reasonable */

	gdb_info.gdb_pid = 0;
	gdb_info.ioc = NULL;
	gdb_info.fd = 0;
	gdb_info.explicit_dirty = FALSE;

	/* Buffer stuff */
	if (gdb_info.buffer) {
		g_free (gdb_info.buffer);
		gdb_info.buffer = NULL;
	}
	
	if (gdb_info.file_name) {
		g_free (gdb_info.file_name);
		gdb_info.file_name = NULL;
	}

	gdb_info.buffer_length = 0;
	gdb_info.buffer_used = 0;
	
	gdb_info.buffer_locals_offset = 0;

	tmp_list = gdb_info.functions;

	for( tmp_list = gdb_info.functions; tmp_list != NULL; tmp_list = g_list_next (tmp_list)) {
		/* clear node */
		GnomeCrashFunction *p_fn = tmp_list->data;

		g_return_if_fail (p_fn);

		g_return_if_fail (p_fn->name);
		g_free (p_fn->name);
		
		g_return_if_fail (p_fn->variables); 
		g_free (p_fn->variables);

		g_free (p_fn);
	}
	
	g_list_free (gdb_info.functions);
	gdb_info.functions = NULL;
}





























































#if 0

void
start_gdb ()
{

     /*
       static CrashType old_type = -1;
     */
/*
	gchar *app   = NULL;
*/
	gchar *extra = NULL;

	g_message (_("obtaining stack trace... (%d)"), 123);

	/*
	extra = gtk_entry_get_text (GTK_ENTRY (CORE_FILE));
	*/

	get_trace_from_core (extra);

}


void 
get_trace_from_core (const gchar *core_file)
{
	gchar *gdb_cmd;
	gchar buf[1024];
	gchar *binary = NULL;
	int status;
	FILE *f;

	gdb_cmd = g_strdup_printf ("gdb --batch --core=%s", core_file);

	f = popen (gdb_cmd, "r");
	g_free (gdb_cmd);

	if (!f) {
		gchar *s = g_strdup_printf (_("Unable to process core file with gdb:\n"
					      "'%s'"), core_file);
		GtkWidget *d = gnome_error_dialog (s);
		g_free (s);
		gnome_dialog_run_and_close (GNOME_DIALOG (d));
		return;
	}

	while (fgets(buf, 1024, f) != NULL) {
		if (!binary && !strncmp(buf, "Core was generated", 16)) {
			gchar *s;
			gchar *ptr = buf;
			while (*ptr != '`' && *ptr !='\0') ptr++;
			if (*ptr == '`') {
				ptr++;
				s = ptr;
				while (*ptr != '\'' && *ptr !=' ' && *ptr !='\0') ptr++;
				*ptr = '\0';
				binary = g_strdup(s);
			}
		}
	}

	status = pclose(f);

	if (!binary) {
		gchar *s = g_strdup_printf (_("Unable to determine which binary created\n"
					      "'%s'"), core_file);
		GtkWidget *d = gnome_error_dialog (s);
		g_free (s);
		gnome_dialog_run_and_close (GNOME_DIALOG (d));
		return;
	}	

	get_trace_from_pair (binary, core_file);
	g_free (binary);
}


#endif
