/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* gnome-crash - small utility that displays a backtrace after a core dump.
 *
 * Copyright (C) 2000 Jose M Celorio
 *
 * Author:  Chema Celorio <chema@celorio.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <libgnomeui/gnome-window-icon.h>

#include "gnome-crash.h"

PoptData popt_data;
GdbInfo gdb_info;

GladeXML  * gui = NULL;
GtkWidget *app;
GtkWidget * stack_box;
GtkWidget * variables_box;
GtkWidget * functions_list;

static const struct poptOption options[] = {
	{ "package",     0, POPT_ARG_STRING, &popt_data.package,     0, N_("Package containing the program"),  N_("PACKAGE") },
	{ "package-ver", 0, POPT_ARG_STRING, &popt_data.package_ver, 0, N_("Version of the package"),          N_("VERSION") },
	{ "appname",     0, POPT_ARG_STRING, &popt_data.app_file,    0, N_("File name of crashed program"),    N_("FILE") },
	{ "pid",         0, POPT_ARG_STRING, &popt_data.pid,         0, N_("PID of crashed program"),          N_("PID") },
	{ "core",        0, POPT_ARG_STRING, &popt_data.core_file,   0, N_("Core file from program"),          N_("FILE") },
	{ NULL } 
};

void gnome_crash_destroy_cb       (GtkWidget *widget, gpointer data);
void gnome_crash_about_cb         (GtkWidget *widget, gpointer data);
void gnome_crash_open_cb          (GtkWidget *widget, gpointer data);
void gnome_crash_save_cb          (GtkWidget *widget, gpointer data);
void gnome_crash_save_as_cb       (GtkWidget *widget, gpointer data);
void gnome_crash_print_cb         (GtkWidget *widget, gpointer data);
void gnome_crash_settings_cb      (GtkWidget *widget, gpointer data);
void gnome_crash_help_cb          (GtkWidget *widget, gpointer data);
void gnome_crash_exit_cb          (GtkWidget *widget, gpointer data);

void
gnome_crash_destroy_cb (GtkWidget *widget, gpointer data)
{
	gnome_crash_gdb_info_clear ();
	gtk_widget_destroy (widget);
	gtk_object_destroy (GTK_OBJECT(gui));
	gtk_main_quit();
}

void
gnome_crash_about_cb (GtkWidget *widget, gpointer data)
{
	GladeXML *gui_about;

	if (!g_file_exists (GNOME_CRASH_GLADE_DIR GNOME_CRASH_GLADE_FILE_ABOUT)) {
		g_warning ("Could not find %s\n",
			   GNOME_CRASH_GLADE_DIR GNOME_CRASH_GLADE_FILE_ABOUT);
		return;
	}
		
	gui_about = glade_xml_new (GNOME_CRASH_GLADE_DIR GNOME_CRASH_GLADE_FILE_ABOUT, NULL);

	if (!gui_about)	{
		g_warning ("Could not find about.glade");
		return;
	}
	
	gtk_widget_show (glade_xml_get_widget (gui_about, "about"));
	gtk_object_destroy (GTK_OBJECT (gui_about));
}


static void
gnome_crash_not_implemented (const gchar *message)
{
	GnomeDialog *dialog;
	gchar *messg;

	messg = g_strdup_printf (_("%s function not yet implemented\n"),
				 message);

	dialog = GNOME_DIALOG (gnome_message_box_new (messg,
						      GNOME_MESSAGE_BOX_INFO,
						      GNOME_STOCK_BUTTON_OK,
						      NULL));

	g_free (messg);

	gnome_dialog_set_parent (GNOME_DIALOG(dialog), GTK_WINDOW(app));
	gnome_dialog_run_and_close (dialog);

}


void gnome_crash_open_cb  (GtkWidget *widget, gpointer data)
{
	/*gnome_crash_not_implemented (_("Open"));*/
	gnome_crash_file_open ();
}

void gnome_crash_exit_cb  (GtkWidget *widget, gpointer data)
{
	gnome_crash_not_implemented (_("Exit"));
}

void gnome_crash_save_cb  (GtkWidget *widget, gpointer data)
{
	gnome_crash_file_save ();
}

void gnome_crash_save_as_cb  (GtkWidget *widget, gpointer data)
{
	gnome_crash_not_implemented (_("Save As"));
}

void gnome_crash_settings_cb  (GtkWidget *widget, gpointer data)
{
	gnome_crash_not_implemented (_("Settings"));
}

void gnome_crash_help_cb  (GtkWidget *widget, gpointer data)
{
	gnome_crash_not_implemented (_("Help"));
}


GnomeApp *
gnome_crash_get_toplevel (GnomeApp *app_in)
{
	static GnomeApp *app = NULL;

	if ((app == NULL) && (app_in == NULL)) {
		g_warning ("App and app in are NULL");
		return NULL;
	}

	if (app == NULL) {
		g_return_val_if_fail (GNOME_IS_APP (app_in), NULL);
		app = app_in;
	}

	g_return_val_if_fail (GNOME_IS_APP (app), NULL);

	return app;
}

static gint
gnome_crash_create_main_window (void)
{
	if (!g_file_exists (GNOME_CRASH_GLADE_DIR GNOME_CRASH_GLADE_FILE)) {
		g_warning ("Could not find %s\n",
			   GNOME_CRASH_GLADE_DIR GNOME_CRASH_GLADE_FILE);
		return FALSE;
	}
	     
	gui = glade_xml_new (GNOME_CRASH_GLADE_DIR GNOME_CRASH_GLADE_FILE, NULL);

	app            = glade_xml_get_widget (gui, "app");
	stack_box      = glade_xml_get_widget (gui, "stack_box");
	variables_box  = glade_xml_get_widget (gui, "variables_box");
	functions_list = glade_xml_get_widget (gui, "functions_list");

	g_return_val_if_fail (app            != NULL, FALSE);
	g_return_val_if_fail (stack_box      != NULL, FALSE);
	g_return_val_if_fail (variables_box  != NULL, FALSE);
	g_return_val_if_fail (functions_list != NULL, FALSE);
	
	glade_xml_signal_autoconnect (gui);

	gnome_crash_get_toplevel (GNOME_APP (app));
	
	gtk_widget_show_all (app);

	return TRUE;
}

static gint
gnome_crash_load_backtrace (void)
{
	if (!popt_data.app_file || !popt_data.pid)
		return FALSE;

	gnome_crash_get_trace_from_pid (popt_data.app_file, popt_data.pid);
	
	return FALSE;
}

static void
gnome_crash_init (void)
{
	gdb_info.file_name     = NULL;
	gdb_info.buffer_length = 0;
	gdb_info.buffer_used   = 0;
	gdb_info.buffer        = NULL;
}

int
main (int argc, char *argv[])
{
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	gnome_init_with_popt_table (PACKAGE, VERSION, argc, argv, 
				    options, 0, NULL);

	gnome_window_icon_set_default_from_file (GNOME_CRASH_ICON_DIR GNOME_CRASH_ICON);

	glade_gnome_init ();

	gnome_crash_init ();
	
	if (!gnome_crash_create_main_window ())
		return 0;

	gtk_idle_add ((GtkFunction) gnome_crash_load_backtrace, NULL);

	gnome_crash_function_connect_signals ();

	gtk_main ();

	return 0;
}
