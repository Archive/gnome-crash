/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* gnome-crash - small utility that displays a backtrace after a core dump.
 *
 * Copyright (C) 2000 Jose M Celorio
 *
 * Author:  Chema Celorio <chema@celorio.com>
 *
 * code taken from gedit 0.9.0pre2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gnome.h>

#include "gnome-crash.h"

void
gnome_crash_show_buffer (void) 
{
	/* Don't know whether my slow video card has sthg. to do
	   but freezing DOES a difference */
	gtk_text_freeze (GTK_TEXT (stack_box));
	
	gtk_editable_delete_text (GTK_EDITABLE (stack_box), 0, -1);
	gtk_text_insert (GTK_TEXT (stack_box), NULL,
			 NULL, NULL, gdb_info.buffer, gdb_info.buffer_length);
	
	gtk_text_thaw (GTK_TEXT (stack_box));
}







