/* gnome program that crashes
 *
 * Copyright (C) Jacob Berkman
 *
 * Author: Jacob Berkman  <jberkman@andrew.cmu.edu>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors :
 *    Jacob Berkman
 *    Chema Celorio
 */

#include "config.h"

#include <gnome.h>

gchar *str;

static gboolean
i_like_to_crash (gchar *foo)
{
	int n;
	GSList *list;

	g_print ("I am about to crash.. .\n");

	n = 5688722;
	str = g_strdup ("Saludar al vecino, acostarse a una hora ...\n");

	list = GINT_TO_POINTER (3332222);
	g_print ("%i", GPOINTER_TO_INT (list->next));

	g_slist_length (list);	

	g_print ("I have crashed..\n");

	return TRUE;
}

static gboolean
first_function (gchar *foo)
{
	gchar *foo_2;
	gint number;
	gint number_2;

	number = 123;
	number_2 = number * 2;
	foo_2 = g_strdup ("www.gnome.org");

	i_like_to_crash ("We deliver more pizzas !\n");

	return TRUE;
}

int
main (int argc, char *argv[])
{
	gnome_client_disable_master_connection ();
	gnome_init (PACKAGE, VERSION, argc, argv);

	first_function ("Shakira Membark Rules !\n");
	
	return 0;
}
