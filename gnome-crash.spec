%define name	gnome-crash
%define ver	0.0.5
%define RELEASE	1
%define rel	%{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}
%define prefix /usr
%define sysconfdir /etc

Summary: Small program that displays the BackTrace infromation after a core dump
Name: %name
Version: %ver
Release: %rel
Copyright: GPL
Group: Applications/System
Source: gnome-crash-%{PACKAGE_VERSION}.tar.gz
URL: http://gnome-crash.sourceforge.net
BuildRoot: /var/tmp/gnome-crahs-root
Docdir: %{prefix}/doc

Requires: gnome-libs >= 1.0.60
Requires: libglade >= 0.5

%description
A small and simple utility to display, store and print the
Backtrace information after a core dump. Usefull for developers
when working with their own programs.

%changelog

* Sat Jul 01 2000 Chema Celorio <chema@celorio.com>

- initial development

%prep
%setup

%build

MYCFLAGS="$RPM_OPT_FLAGS"

if [ ! -f configure ]; then
  CFLAGS="$MYCFLAGS" ./autogen.sh $MYARCH_FLAGS --prefix=%prefix --sysconfdir=%sysconfdir --localstatedir=/var/lib
else
  CFLAGS="$MYCFLAGS" ./configure $MYARCH_FLAGS --prefix=%prefix --sysconfdir=%sysconfdir --localstatedir=/var/lib
fi

if [ "$SMP" != "" ]; then
  make -j$SMP "MAKE=make -j$SMP"
else
  make
fi

%install
rm -fr $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -fr $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc ABOUT-NLS AUTHORS ChangeLog COPYING INSTALL NEWS README TODO
%{prefix}/bin/*
%{prefix}/share/pixmaps/*
%{prefix}/share/gnome/*
%{prefix}/share/mime-info/*
%{prefix}/share/gnome-crash
%{prefix}/share/locale/*
